package com.test.myproject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void testAdd() {
    	assertEquals(7,Calculator.add(2, 5));
    }
    
    @Test 
    public void testSub() {
    	assertEquals(10,Calculator.sub(5, 15));
    }
    
}
